package triskeldb.storage;

import java.util.ArrayList;
import org.junit.Test;
import triskeldb.storage.factstorage.Cube;
import static org.junit.Assert.*;
import triskeldb.facts.Fact;

public class FactStorageTest {

    private FactStorage initFactStorage() {
        FactStorage factStorage = new FactStorage();
        Fact numericFactOne = new Fact(1, 2, 3);
        Fact numericFactTwo = new Fact(0, 2, 3);
        Fact numericFactThree = new Fact(2, 2, 3);
        Fact numericFactFour = new Fact(3, 2, 9);
        factStorage.add(numericFactOne);
        factStorage.add(numericFactTwo);
        factStorage.add(numericFactThree);
        factStorage.add(numericFactFour);
        return factStorage;
    }

    @Test
    public void createAndCheckFactStorage() throws Exception {
        FactStorage factStorage = new FactStorage();
        assertEquals(Cube.class, factStorage.getStorageState().getClass());
    }

    @Test
    public void createAndCheckRangePatterns() {
        FactStorage factStorage = initFactStorage();
        ArrayList<Integer> idRange = new ArrayList<>();
        idRange.add(3);
        idRange.add(4);
        idRange.add(9);
        ArrayList<Integer> subjectObtained = factStorage.matchParcialSubject(2, idRange);
        ArrayList<Integer> subjectExpected = new ArrayList<>();
        subjectExpected.add(0);
        subjectExpected.add(1);
        subjectExpected.add(2);
        subjectExpected.add(3);
        assertEquals(subjectExpected.size(), subjectObtained.size());
    }
}