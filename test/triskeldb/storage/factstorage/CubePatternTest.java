package triskeldb.storage.factstorage;

import org.junit.Test;
import static org.junit.Assert.*;
import triskeldb.facts.Fact;

public class CubePatternTest {

    private Cube initializeTestCube() throws Exception {
        Cube cube = new Cube(5, 5, 5);
        Fact fact1 = new Fact(0, 0, 0);
        Fact fact2 = new Fact(1, 1, 1);
        Fact fact3 = new Fact(2, 2, 2);
        cube.add(fact1);
        cube.add(fact2);
        cube.add(fact3);
        return cube;
    }

    @Test
    public void checkIfMatchACompletePattern() throws Exception {
        Cube cube = initializeTestCube();
        assertTrue(cube.matchPattern(0, 0, 0));
    }

    @Test
    public void checkIfMatchAPartialSubjectPattern() throws Exception {
        Cube cube = initializeTestCube();
        assertEquals(1, cube.matchParcialSubject(0, 0).size());
    }

    @Test
    public void checkIfMatchAPartialPredicatePattern() throws Exception {
        Cube cube = initializeTestCube();
        assertEquals(1, cube.matchParcialPredicate(1, 1).size());
    }

    @Test
    public void checkIfMatchAPartialObjectPattern() throws Exception {
        Cube cube = initializeTestCube();
        assertEquals(1, cube.matchParcialObject(2, 2).size());
    }

    @Test
    public void matchParcialPredicateObject() throws Exception {
        Cube cube = initializeTestCube();
        assertEquals(1, cube.matchParcialPredicateObject(0).size());
    }

    @Test
    public void matchParcialSubjectObject() throws Exception {
        Cube cube = initializeTestCube();
        assertEquals(1, cube.matchParcialSubjectObject(1).size());
    }

    @Test
    public void matchParcialSubjectPredicate() throws Exception {
        Cube cube = initializeTestCube();
        assertEquals(1, cube.matchParcialSubjectPredicate(2).size());
    }

    @Test
    public void matchTotal() throws Exception {
        Cube cube = initializeTestCube();
        assertEquals(3, cube.matchTotal().size());
    }
}