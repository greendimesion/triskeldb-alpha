package triskeldb.storage.factstorage;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import triskeldb.facts.Fact;

public class CubeTest {

    private static Cube cubeOne;
    private static Cube cubeTwo;
    private static Cube cubeZero;

    @Before
    public void createCubes() {
        try {
            cubeOne = new Cube(5, 2, 5);
            cubeTwo = new Cube(4, 1, 4);
        } catch (Exception e) {
            System.out.println("Error creating cubes for test");
        }
    }

    @Test
    public void checkNewZeroSubjectsCube() {
        try {
            cubeZero = new Cube(0, 3, 5);
            assertTrue(false);
        } catch (Exception e) {
            assertTrue(true);
        }
    }

    @Test
    public void checkNewZeroPredicatesCube() {
        try {
            cubeZero = new Cube(5, 0, 5);
            assertTrue(false);
        } catch (Exception e) {
            assertTrue(true);
        }
    }

    @Test
    public void checkNewZeroObjectsCube() {
        try {
            cubeZero = new Cube(5, 3, 0);
            assertTrue(false);
        } catch (Exception e) {
            assertTrue(true);
        }
    }

    @Test
    public void checkNewCube() {
        assertEquals(cubeOne.getClass(), Cube.class);
        assertNotNull(cubeOne);
    }

    @Test
    public void NumberOfSubjects() {
        assertEquals(cubeOne.getNumberOfSubjects(), 5, 0);
    }

    @Test
    public void NumberOfSubjectsOtherCube() {
        assertEquals(cubeTwo.getNumberOfSubjects(), 4, 0);
    }

    @Test
    public void NumberOfPredicates() {
        assertEquals(cubeOne.getNumberOfPredicates(), 2, 0);
    }

    @Test
    public void NumberOfPredicatesOtherCube() {
        assertEquals(cubeTwo.getNumberOfPredicates(), 1, 0);
    }

    @Test
    public void NumberOfObjects() {
        assertEquals(cubeOne.getNumberOfObjects(), 5, 0);
    }

    @Test
    public void NumberOfObjectsOtherCube() {
        assertEquals(cubeTwo.getNumberOfObjects(), 4, 0);
    }

    @Test
    public void noExistsFact() {
        assertFalse(cubeOne.matchPattern(2, 1, 1));
    }

    @Test
    public void yesExistsFact() {
        Fact fact = new Fact(1, 1, 1);
        cubeOne.add(fact);
        assertTrue(cubeOne.matchPattern(1, 1, 1));
    }

    @Test
    public void addFact() {
        Fact fact = new Fact(1, 1, 1);
        cubeOne.add(fact);
        assertTrue(cubeOne.matchPattern(1, 1, 1));
    }

    @Test
    public void extractFact() {
        Fact fact = new Fact(1, 1, 1);
        cubeOne.extract(fact);
        assertFalse(cubeOne.matchPattern(1, 1, 1));
    }
}