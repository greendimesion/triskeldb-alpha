package triskeldb.queries.querytranslator;

import triskeldb.queries.query.PlainQuery;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import triskeldb.facts.Fact;
import triskeldb.facts.translator.Library;
import triskeldb.facts.translator.Translator;

public class QueryTranslatorTest {

    private void initTranslator() {
        Translator translator = Translator.getInstance();
        Library library = Library.getInstance();
        library.addSubject("Pepe");
        library.addPredicate("has");
        library.addObject("luck");
    }

    @Test
    public void translatePlainSimpleQueryIntoFact() {
        initTranslator();
        QueryTranslator queryTranslator = new QueryTranslator();
        PlainQuery simpleQuery = new PlainQuery("Pepe", "has", "luck");
        Fact factExpected = new Fact(0, 1, 2);
        Fact fact;
        fact = queryTranslator.simpleTranslate(simpleQuery);
        assertEquals(factExpected.getSubject(), fact.getSubject());
    }

    @After
    public void translateMultipleIdQueriesintoMultiplePlainQueries() {
        initTranslator();
        QueryTranslator queryTranslator = new QueryTranslator();
        ArrayList<Fact> factList = new ArrayList<>();
        factList.add(new Fact(0, 1, 1));
        factList.add(new Fact(1, 1, 1));
        ArrayList<PlainQuery> plainQueryList = queryTranslator.translateIdQueries(factList);
        assertEquals(factList.size(), plainQueryList.size());
    }
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void checkInvalidQueryParameters() {
        initTranslator();
        QueryTranslator queryTranslator = new QueryTranslator();
        exception.expect(NullPointerException.class);
        queryTranslator.simpleTranslate(new PlainQuery("Pepe", "has", "badluck"));

    }

    @Before
    public void translatePlainComplexQueryIntoMultipleFacts() {
        Translator translator = Translator.getInstance();
        Library library = Library.getInstance();
        library.addSubject("Pepe");
        library.addPredicate("is");
        library.addObject("rich");
        library.addSubject("Juan");
        library.addPredicate("is");
        library.addObject("rich");
        QueryTranslator queryTranslator = new QueryTranslator();
        ArrayList<Fact> factList = queryTranslator.complexTranslate(new PlainQuery("?", "is", "rich"));
        assertTrue(!factList.isEmpty());
    }
}
