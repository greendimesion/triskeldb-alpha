package triskeldb.queries.operation;

import org.junit.Test;
import static org.junit.Assert.*;
import triskeldb.queries.query.PlainQuery;
import triskeldb.storage.MatchingManager;

public class OperationIntersectionTest extends OperationTest {

    @Test
    public void intersectionPointToPointWithSameValues() {
        PlainQuery firstQuery = new PlainQuery("Aitor", "is", "rich");
        PlainQuery secondQuery = new PlainQuery("Aitor", "is", "rich");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        assertFalse(operation.intersection(firstQuery, secondQuery).isEmpty());
    }

    @Test
    public void intersectionPointToPointWithDifferentValues() {
        PlainQuery firstQuery = new PlainQuery("Aitor", "is", "man");
        PlainQuery secondQuery = new PlainQuery("Roman", "is", "rich");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        assertTrue(operation.intersection(firstQuery, secondQuery).isEmpty());
    }

    @Test
    public void intersectionLineToPointWhatSubject() {
        PlainQuery firstQuery = new PlainQuery("?", "is", "rich");
        PlainQuery secondQuery = new PlainQuery("Aitor", "is", "rich");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        assertEquals(1, operation.intersection(firstQuery, secondQuery).size());
    }

    @Test
    public void intersectionPointToLineWhatSubject() {
        PlainQuery firstQuery = new PlainQuery("Aitor", "is", "rich");
        PlainQuery secondQuery = new PlainQuery("?", "is", "rich");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        assertEquals(1, operation.intersection(firstQuery, secondQuery).size());
    }

    @Test
    public void intersectionLineToPointWhatPredicate() {
        PlainQuery firstQuery = new PlainQuery("Aitor", "?", "rich");
        PlainQuery secondQuery = new PlainQuery("Aitor", "is", "rich");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        assertEquals(1, operation.intersection(firstQuery, secondQuery).size());
    }

    @Test
    public void intersectionPointToLineWhatPredicate() {
        PlainQuery firstQuery = new PlainQuery("Aitor", "is", "rich");
        PlainQuery secondQuery = new PlainQuery("Aitor", "?", "rich");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        assertEquals(1, operation.intersection(firstQuery, secondQuery).size());
    }

    @Test
    public void intersectionLineToPointWhatObject() {
        PlainQuery firstQuery = new PlainQuery("Aitor", "is", "?");
        PlainQuery secondQuery = new PlainQuery("Aitor", "is", "rich");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        assertEquals(1, operation.intersection(firstQuery, secondQuery).size());
    }

    @Test
    public void intersectionPointToLineWhatObject() {
        PlainQuery firstQuery = new PlainQuery("Aitor", "is", "rich");
        PlainQuery secondQuery = new PlainQuery("Aitor", "is", "?");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        assertEquals(1, operation.intersection(firstQuery, secondQuery).size());
    }

    @Test
    public void intersectionLineToLineWithSameValues() {
        PlainQuery firstQuery = new PlainQuery("?", "is", "rich");
        PlainQuery secondQuery = new PlainQuery("?", "is", "rich");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        assertEquals(3, operation.intersection(firstQuery, secondQuery).size());
    }

    @Test
    public void intersectionLineToLineWithDifferentValues() {
        PlainQuery firstQuery = new PlainQuery("Aitor", "?", "rich");
        PlainQuery secondQuery = new PlainQuery("?", "is", "rich");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        assertEquals(1, operation.intersection(firstQuery, secondQuery).size());
    }

    @Test
    public void intersectionPointToPlane() {
        PlainQuery firstQuery = new PlainQuery("Aitor", "is", "rich");
        PlainQuery secondQuery = new PlainQuery("?", "is", "?");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        assertEquals(1, operation.intersection(firstQuery, secondQuery).size());
    }

    @Test
    public void intersectionPlaneToPoint() {
        PlainQuery firstQuery = new PlainQuery("?", "is", "?");
        PlainQuery secondQuery = new PlainQuery("Aitor", "is", "rich");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        assertEquals(1, operation.intersection(firstQuery, secondQuery).size());
    }

    @Test
    public void intersectionLineToPlane() {
        PlainQuery firstQuery = new PlainQuery("?", "is", "rich");
        PlainQuery secondQuery = new PlainQuery("?", "is", "?");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        assertEquals(3, operation.intersection(firstQuery, secondQuery).size());
    }

    @Test
    public void intersectionPlaneToLine() {
        PlainQuery firstQuery = new PlainQuery("?", "is", "?");
        PlainQuery secondQuery = new PlainQuery("?", "is", "rich");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        assertEquals(3, operation.intersection(firstQuery, secondQuery).size());
    }

    @Test
    public void intersectionPlaneToLineResultOnePoint() {
        PlainQuery firstQuery = new PlainQuery("?", "is", "?");
        PlainQuery secondQuery = new PlainQuery("Aitor", "?", "rich");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        assertEquals(1, operation.intersection(firstQuery, secondQuery).size());
    }

    @Test
    public void intersectionParallelPlaneToPlane() {
        PlainQuery firstQuery = new PlainQuery("?", "is", "?");
        PlainQuery secondQuery = new PlainQuery("?", "becomes", "?");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        assertEquals(0, operation.intersection(firstQuery, secondQuery).size());
    }

    @Test
    public void intersectionParallelLineToLine() {
        PlainQuery firstQuery = new PlainQuery("?", "is", "rich");
        PlainQuery secondQuery = new PlainQuery("?", "becomes", "man");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        assertEquals(0, operation.intersection(firstQuery, secondQuery).size());
    }

    @Test
    public void intersectionPlaneToPlane() {
        PlainQuery firstQuery = new PlainQuery("?", "is", "?");
        PlainQuery secondQuery = new PlainQuery("?", "is", "?");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        assertEquals(3, operation.intersection(firstQuery, secondQuery).size());
    }
}