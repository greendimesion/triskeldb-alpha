package triskeldb.queries.operation;

import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;
import triskeldb.queries.query.PlainQuery;
import triskeldb.queries.querytranslator.QueryTranslator;
import triskeldb.storage.MatchingManager;

public class OperationProyectionTest extends OperationTest {

    @Test
    public void proyectionSubject() {

        PlainQuery firstQuery = new PlainQuery("?", "becomes", "man");
        QueryTranslator queryTranslator = new QueryTranslator();
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        ArrayList<Integer> proyectionResult = operation.proyectionSubject(queryTranslator.complexTranslate(firstQuery));
        assertEquals(4, proyectionResult.size());

    }

    @Test
    public void proyectionPredicate() {

        PlainQuery firstQuery = new PlainQuery("Xerach", "?", "man");
        QueryTranslator queryTranslator = new QueryTranslator();
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        ArrayList<Integer> proyectionResult = operation.proyectionPredicate(queryTranslator.complexTranslate(firstQuery));
        assertEquals(3, proyectionResult.size());

    }

    @Test
    public void proyectionObject() {

        PlainQuery firstQuery = new PlainQuery("Xerach", "becomes", "?");
        QueryTranslator queryTranslator = new QueryTranslator();
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        ArrayList<Integer> proyectionResult = operation.proyectionObject(queryTranslator.complexTranslate(firstQuery));
        assertEquals(4, proyectionResult.size());

    }
}