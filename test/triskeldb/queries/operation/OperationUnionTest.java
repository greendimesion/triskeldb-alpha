package triskeldb.queries.operation;

import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;
import triskeldb.queries.query.PlainQuery;
import triskeldb.storage.MatchingManager;

public class OperationUnionTest extends OperationTest {

    @Test
    public void simpleQueriesUnion() {

        PlainQuery firstQuery = new PlainQuery("Aitor", "becomes", "man");
        PlainQuery secondQuery = new PlainQuery("Roman", "is", "rich");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        ArrayList<PlainQuery> unionResult = operation.union(firstQuery, secondQuery);
        ArrayList<PlainQuery> unionExpectedResult = new ArrayList<>();
        unionExpectedResult.add(firstQuery);
        assertEquals(unionExpectedResult.size(), unionResult.size());

    }

    @Test
    public void complexQueriesUnionOneQuestionMark() {
        PlainQuery firstQuery = new PlainQuery("?", "becomes", "man");
        PlainQuery secondQuery = new PlainQuery("?", "is", "rich");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        ArrayList<PlainQuery> unionResult = operation.union(firstQuery, secondQuery);
        assertEquals(5, unionResult.size());
    }

    @Test
    public void complexQueriesUnionDoubleQuestionMark() {
        PlainQuery firstQuery = new PlainQuery("?", "?", "man");
        PlainQuery secondQuery = new PlainQuery("?", "?", "rich");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        ArrayList<PlainQuery> unionResult = operation.union(firstQuery, secondQuery);
        assertEquals(5, unionResult.size());
    }

    @Test
    public void complexQueriesUnionOneAndDoubleQuestionMark() {
        PlainQuery firstQuery = new PlainQuery("?", "is", "man");
        PlainQuery secondQuery = new PlainQuery("?", "?", "rich");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        ArrayList<PlainQuery> unionResult = operation.union(firstQuery, secondQuery);
        assertEquals(3, unionResult.size());
    }

    @Test
    public void simpleQueryWithComplexQueryOneQuestionMark() {
        PlainQuery firstQuery = new PlainQuery("Aitor", "becomes", "man");
        PlainQuery secondQuery = new PlainQuery("?", "is", "rich");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        ArrayList<PlainQuery> unionResult = operation.union(firstQuery, secondQuery);
        assertEquals(4, unionResult.size());
    }

    @Test
    public void simpleQueryWithComplexQueryDoubleQuestionMark() {
        PlainQuery firstQuery = new PlainQuery("Aitor", "becomes", "man");
        PlainQuery secondQuery = new PlainQuery("?", "?", "rich");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        ArrayList<PlainQuery> unionResult = operation.union(firstQuery, secondQuery);
        assertEquals(4, unionResult.size());
    }

    // shall method union duplicate the result when it exist in both sets?
    @Test
    public void complexQueriesUnionTripleQuestionMark() {
        PlainQuery firstQuery = new PlainQuery("?", "?", "?");
        PlainQuery secondQuery = new PlainQuery("?", "?", "?");
        MatchingManager matcher = initContext();
        Operation operation = new Operation(matcher);
        ArrayList<PlainQuery> unionResult = operation.union(firstQuery, secondQuery);
        assertEquals(14, unionResult.size());
    }
}