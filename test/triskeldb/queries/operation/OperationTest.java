package triskeldb.queries.operation;

import org.junit.Test;
import triskeldb.facts.Fact;
import triskeldb.facts.translator.Library;
import triskeldb.facts.translator.Translator;
import triskeldb.storage.FactStorage;
import triskeldb.storage.MatchingManager;
import triskeldb.storage.StorageManager;

public class OperationTest {

    private Translator translator = Translator.getInstance();
    private Library library = Library.getInstance();

    MatchingManager initContext() {
        initTranslator();
        return (MatchingManager) initStorage();
    }

    private void initTranslator() {
        library.addSubject("Xerach");
        library.addPredicate("is");
        library.addObject("hero");
        library.addSubject("David");
        library.addSubject("Roman");
        library.addPredicate("seems");
        library.addObject("man");
        library.addSubject("Aitor");
        library.addPredicate("becomes");
        library.addObject("ugly");
        library.addObject("rich");
    }

    private StorageManager initStorage() {
        StorageManager factStorage = new FactStorage();
        factStorage.add(new Fact(translator.translateSubject("Xerach"), translator.translatePredicate("is"), translator.translateObject("rich")));
        factStorage.add(new Fact(translator.translateSubject("Aitor"), translator.translatePredicate("is"), translator.translateObject("rich")));
        factStorage.add(new Fact(translator.translateSubject("Roman"), translator.translatePredicate("becomes"), translator.translateObject("man")));
        factStorage.add(new Fact(translator.translateSubject("David"), translator.translatePredicate("is"), translator.translateObject("rich")));
        factStorage.add(new Fact(translator.translateSubject("Xerach"), translator.translatePredicate("seems"), translator.translateObject("hero")));
        factStorage.add(new Fact(translator.translateSubject("Roman"), translator.translatePredicate("becomes"), translator.translateObject("ugly")));
        factStorage.add(new Fact(translator.translateSubject("Aitor"), translator.translatePredicate("becomes"), translator.translateObject("man")));
        return factStorage;
    }

    @Test
    public void opetarionTest() {
        assert (true);
    }
}