package triskeldb.queries.query;

import junit.framework.Assert;
import org.junit.Test;

public class QueryTest {

    private Query initQuery() {
        String[] subject = {"Pepe"};
        String[] predicate = {"has"};
        String[] directObject = {"luck", "smart", "fast"};
        return new Query(subject, predicate, directObject);
    }

    @Test
    public void createMultipleQueriesAndCheckFirstCreated() {
        Query query = initQuery();
        PlainQuery firstSimpleQueryExpected = new PlainQuery("Pepe", "has", "luck");
        PlainQuery firstSimpleQueryObtained = query.nextPlainQuery();
        Assert.assertEquals(firstSimpleQueryExpected.getSubject(), firstSimpleQueryObtained.getSubject());
        Assert.assertEquals(firstSimpleQueryExpected.getPredicate(), firstSimpleQueryObtained.getPredicate());
        Assert.assertEquals(firstSimpleQueryExpected.getObject(), firstSimpleQueryObtained.getObject());
    }

    @Test
    public void createMultipleQueriesAndCheckSecondCreated() {
        Query query = initQuery();
        PlainQuery secondSimpleQueryExpected = new PlainQuery("Pepe", "has", "smart");
        query.nextPlainQuery();
        PlainQuery secondSimpleQueryObtained = query.nextPlainQuery();
        Assert.assertEquals(secondSimpleQueryExpected.getSubject(), secondSimpleQueryObtained.getSubject());
        Assert.assertEquals(secondSimpleQueryExpected.getPredicate(), secondSimpleQueryObtained.getPredicate());
        Assert.assertEquals(secondSimpleQueryExpected.getObject(), secondSimpleQueryObtained.getObject());
    }

    @Test
    public void createAndCheckMultipleQueries() {
        Query query = initQuery();
        PlainQuery thirdSimpleQueryExpected = new PlainQuery("Pepe", "has", "fast");
        query.nextPlainQuery();
        query.nextPlainQuery();
        PlainQuery thirdSimpleQueryObtained = query.nextPlainQuery();
        Assert.assertEquals(thirdSimpleQueryExpected.getSubject(), thirdSimpleQueryObtained.getSubject());
        Assert.assertEquals(thirdSimpleQueryExpected.getPredicate(), thirdSimpleQueryObtained.getPredicate());
        Assert.assertEquals(thirdSimpleQueryExpected.getObject(), thirdSimpleQueryObtained.getObject());
    }

    @Test
    public void existMoreQueries() {
        Query query = initQuery();
        Assert.assertTrue(query.areThereMoreQueries());
    }

    @Test
    public void notExistMoreQueries() {
        Query query = initQuery();
        query.nextPlainQuery();
        query.nextPlainQuery();
        query.nextPlainQuery();
        Assert.assertFalse(query.areThereMoreQueries());
    }
}