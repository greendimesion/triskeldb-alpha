package triskeldb.queries.query;

import org.junit.Test;
import static org.junit.Assert.*;

public class ThreeElementsTupleTest {

    @Test
    public void createAndCheckAThreeElementsTuple() {
        ThreeElementsTuple tuple = new ThreeElementsTuple(1, 1, 1);
        assertEquals(1, tuple.getFirstValue());
        assertEquals(1, tuple.getSecondValue());
        assertEquals(1, tuple.getThirdValue());
    }
}