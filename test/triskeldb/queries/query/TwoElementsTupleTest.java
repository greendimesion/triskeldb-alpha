package triskeldb.queries.query;

import org.junit.Test;
import static org.junit.Assert.*;

public class TwoElementsTupleTest {

    @Test
    public void createAndCheckATwoElementsTuple() {
        TwoElementsTuple tuple = new TwoElementsTuple(1, 1);
        assertEquals(1, tuple.getFirstValue());
        assertEquals(1, tuple.getSecondValue());
    }
}