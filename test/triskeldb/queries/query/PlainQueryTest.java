package triskeldb.queries.query;

import org.junit.Test;
import static org.junit.Assert.*;

public class PlainQueryTest {

    private PlainQuery initSimpleQuery() {
        return new PlainQuery("Pepe", "has", "luck");
    }

    @Test
    public void createAndCheckASimpleQuerySubject() {
        PlainQuery simpleQuery = initSimpleQuery();
        assertEquals("Pepe", simpleQuery.getSubject());
    }

    @Test
    public void createAndCheckASimpleQueryPredicate() {
        PlainQuery simpleQuery = initSimpleQuery();
        assertEquals("has", simpleQuery.getPredicate());
    }

    @Test
    public void createAndCheckASimpleQueryObject() {
        PlainQuery simpleQuery = initSimpleQuery();
        assertEquals("luck", simpleQuery.getObject());
    }

    @Test
    public void checkIfQueryIsNotComplex() {
        PlainQuery simplequery = initSimpleQuery();
        assertFalse(simplequery.isComplex());
    }

    @Test
    public void checkIfQueryIsComplex() {
        PlainQuery simplequery = new PlainQuery("Pepe", "has", "?");
        assertTrue(simplequery.isComplex());
    }
}