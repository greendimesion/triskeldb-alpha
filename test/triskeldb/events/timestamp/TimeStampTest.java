package triskeldb.events.timestamp;

import junit.framework.Assert;
import org.junit.Test;

public class TimeStampTest {

    @Test
    public void timeStampValue1() {
        TimeStamp timeStamp = new TimeStamp();
        Assert.assertEquals(1.0, timeStamp.getSystemNumber());
    }

    @Test
    public void timeStampValue2() {
        TimeStamp timeStamp = new TimeStamp();
        Assert.assertEquals(2.0, timeStamp.getSystemNumber());
    }
}