package triskeldb.events.triplet;

import org.junit.Assert;
import org.junit.Test;

public class TripletTest {

    @Test
    public void tripletSubjectTest() {
        Triplet triplet = initTriplet("Dog", "", "");
        Assert.assertEquals("Dog", triplet.getSubjectValue());
    }

    @Test
    public void tripletPredicateTest() {
        Triplet triplet = initTriplet("", "has", "");
        Assert.assertEquals("has", triplet.getPredicateValue());
    }

    @Test
    public void tripletDirectObjectTest() {
        Triplet triplet = initTriplet("", "", "tail");
        Assert.assertEquals("tail", triplet.getObjectValue());
    }

    @Test
    public void tripletTest() {
        Triplet triplet = initTriplet("Dog", "has", "tail");
        Assert.assertEquals("Dog", triplet.getSubjectValue());
        Assert.assertEquals("has", triplet.getPredicateValue());
        Assert.assertEquals("tail", triplet.getObjectValue());
    }

    @Test
    public void createAndCheckAStringTriplet() {
        Triplet stringTriplet = new Triplet("Code", "is", "clean");
        Assert.assertEquals("Code", stringTriplet.getSubjectValue());
        Assert.assertEquals("is", stringTriplet.getPredicateValue());
        Assert.assertEquals("clean", stringTriplet.getObjectValue());
    }

    private Triplet initTriplet(String s, String p, String o) {
        Subject subject = new Subject(s);
        Predicate predicate = new Predicate(p);
        Object directObject = new Object(o);
        return new Triplet(subject, predicate, directObject);
    }
}