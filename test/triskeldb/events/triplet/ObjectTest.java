package triskeldb.events.triplet;

import junit.framework.Assert;
import org.junit.Test;

public class ObjectTest {

    @Test
    public void createAndCheckNewObject() {
        Object directObject = new Object("car");
        Assert.assertEquals("car", directObject.getObject());
    }

    @Test
    public void createAndCheckOtherObject() {
        Object directObject = new Object("motorbike");
        Assert.assertEquals("motorbike", directObject.getObject());
    }
}