package triskeldb.events.triplet;

import junit.framework.Assert;
import org.junit.Test;

public class SubjectTest {

    @Test
    public void createAndCheckNewSubject() {
        Subject subject = new Subject("Pepe");
        Assert.assertEquals("Pepe", subject.getSubject());
    }

    @Test
    public void createAndCheckOtherSubject() {
        Subject subject = new Subject("house");
        Assert.assertEquals("house", subject.getSubject());
    }
}