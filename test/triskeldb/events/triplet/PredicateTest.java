package triskeldb.events.triplet;

import junit.framework.Assert;
import org.junit.Test;

public class PredicateTest {

    @Test
    public void createAndCheckNewPredicate() {
        Predicate predicate = new Predicate("has");
        Assert.assertEquals("has", predicate.getPredicate());
    }

    @Test
    public void createAndCheckOtherPredicate() {
        Predicate predicate = new Predicate("flys");
        Assert.assertEquals("flys", predicate.getPredicate());
    }
}