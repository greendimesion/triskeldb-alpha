package triskeldb.events.event;

import triskeldb.events.actions.Action;
import triskeldb.events.timestamp.TimeStamp;
import triskeldb.events.triplet.Triplet;
import org.junit.Assert;
import org.junit.Test;

public class DataEventTest {

    @Test
    public void thisEventHasATriplet() {
        DataEvent event = new DataEvent("Code", "is", "clean", 'A');
        Assert.assertEquals(Triplet.class, event.getTriplet().getClass());
    }

    @Test
    public void thisEventHasAnAction() {
        DataEvent event = new DataEvent("Code", "is", "clean", 'A');
        Assert.assertEquals(Action.class, event.getAction().getClass());
    }

    @Test
    public void thisEventHasATimeStamp() {
        DataEvent event = new DataEvent("Code", "is", "clean", 'A');
        Assert.assertEquals(TimeStamp.class, event.getTimeStamp().getClass());
    }
}
