package triskeldb.events.actions;

import junit.framework.Assert;
import org.junit.Test;

public class ActionsTest {

    @Test
    public void addActionTest() {
        Action action = new Action('A');
        Assert.assertEquals('A', action.getActionValue());
    }

    @Test
    public void extractActionTest() {
        Action action = new Action('E');
        Assert.assertEquals('E', action.getActionValue());
    }
}