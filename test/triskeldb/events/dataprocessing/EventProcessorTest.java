package triskeldb.events.dataprocessing;

import triskeldb.events.event.DataEvent;
import java.io.IOException;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import triskeldb.events.event.SnapShotEvent;

public class EventProcessorTest {

    private EventProcessor file;

    private void initTest() {

        file = EventProcessor.getInstance();

    }

    private void insertEventsInFileAndFIFOList() throws IOException {
        DataEvent dataEventevent = new DataEvent("Odin", "has", "son", 'A');
        file.add(dataEventevent);
        DataEvent dataEventevent2 = new DataEvent("Odin", "is", "boy", 'A');
        file.add(dataEventevent2);
        DataEvent dataEventevent3 = new DataEvent("Odin", "has", "son", 'E');
        file.add(dataEventevent3);
        SnapShotEvent snapshotEvent = new SnapShotEvent();
        file.add(snapshotEvent);
    }

    @Before
    public void emptyList() {
        initTest();
        assertTrue(file.readNextEvent() == null);
    }

    @Test
    public void writeAndReadAllEventInFIFOList() {
        try {
            insertEventsInFileAndFIFOList();
            assertTrue(file.readNextEvent() instanceof DataEvent);
            assertTrue(file.readNextEvent() instanceof DataEvent);
            assertTrue(file.readNextEvent() instanceof DataEvent);
            assertFalse(file.readNextEvent() instanceof SnapShotEvent);
            assertEquals(4, file.getTotalElements());
        } catch (IOException ex) {
            System.out.println("Mistake in WriteAndReadAllEventInFile :" + ex.getMessage());
        }
    }

    @After
    public void elementsInFile() throws IOException {
        DataEvent event = new DataEvent("Odin", "has", "son", 'A');
        try {
            file.add(event);
        } catch (Exception ex) {
            System.out.println("Error in sizeFile, type IOException : " + ex.getMessage());
        }
        assertTrue(file.readNextEvent() instanceof DataEvent);
        assertEquals(5, file.getTotalElements());
    }
}