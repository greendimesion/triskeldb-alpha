package triskeldb.events.eventsgenerator;

import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.Test;
import static org.junit.Assert.*;

public class EventsGeneratorTest {

    EventsGenerator eventsGenerator;

    public EventsGeneratorTest() throws IOException {
        this.eventsGenerator = new EventsGenerator();
        eventsGenerator.generate(10);
    }

    @Test
    public void generateTenEvents() throws FileNotFoundException, IOException {
        assertTrue(eventsGenerator.eventsGenerated() == 10);
    }
}