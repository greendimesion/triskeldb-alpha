package triskeldb.facts.translator;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

public class TranslatorTest {

    @Before
    public void introduceStrings() {
        Translator translator = Translator.getInstance();
        Library library = Library.getInstance();
        library.addSubject("pepe");
        library.addSubject("Juan");
    }

    @Test
    public void checkTranslator() {
        Translator translator = Translator.getInstance();
        assertEquals(translator.getClass(), Translator.class);
        assertNotNull(translator);
    }

    @Test
    public void translateString() {
        Translator translator = Translator.getInstance();
        assertEquals(translator.translateSubject("pepe"), 0, 0);
    }

    @Test
    public void translateOtherString() {
        Translator translator = Translator.getInstance();
        assertEquals(translator.translateSubject("Juan"), 1, 0);
    }

    @Test
    public void translateInvalidString() {
        Translator translator = Translator.getInstance();
        assertNull(translator.translateSubject("Cris"));
    }

    @Test
    public void translateID() {
        Translator translator = Translator.getInstance();
        assertEquals(translator.translateSubject(0), "pepe");
    }

    @Test
    public void translateOtherID() {
        Translator translator = Translator.getInstance();
        assertEquals(translator.translateSubject(1), "Juan");
    }

    @Test
    public void translateInvalidID() {
        Translator translator = Translator.getInstance();
        assertNull(translator.translateSubject(-1));
    }

    @Test
    public void actualID() {
        Translator translator = Translator.getInstance();
        Library library = Library.getInstance();
        assertEquals(library.getIDSubject(), 2, 0);
    }
}