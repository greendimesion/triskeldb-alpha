package triskeldb.facts.translator;

import org.junit.Test;
import static org.junit.Assert.*;

public class LibraryTest {

    @Test
    public void loadDictionaries() {
        TranslationsDictionary tdIDtoString = new TranslationsDictionary<Integer, String>();
        TranslationsDictionary tdStringtoID = new TranslationsDictionary<String, Integer>();
        Library library = Library.getInstance();
        library.setIdToStringSubject(tdIDtoString);
        library.setStringToIdSubject(tdStringtoID);
        library.addSubject("Juana");
        assertEquals(library.getIDSubject(), 1, 0);
        assertEquals(library.translateSubject("Juana"), 0, 0);
        assertNull(library.translateSubject("Juan"));

        tdIDtoString = new TranslationsDictionary<Integer, String>();
        tdStringtoID = new TranslationsDictionary<String, Integer>();
        library.setIdToStringSubject(tdIDtoString);
        library.setStringToIdSubject(tdStringtoID);
    }

    @Test
    public void getDictionaries() {
        Library library = Library.getInstance();
        assertNotNull(library.getIdToStringSubject());
        assertNotNull(library.getIdToStringPredicates());
        assertNotNull(library.getIdToStringObjects());

        assertNotNull(library.getStringToIdSubject());
        assertNotNull(library.getStringToIdPredicates());
        assertNotNull(library.getStringToIdObjects());

    }
}