package triskeldb.facts;

import java.io.IOException;
import org.junit.After;
import triskeldb.events.event.DataEvent;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import static org.mockito.Mockito.*;
import triskeldb.events.actions.Action;
import triskeldb.events.dataprocessing.EventProcessor;
import triskeldb.events.event.SnapShotEvent;
import triskeldb.storage.FactStorage;
//import triskeldb.events.dataprocessing.DataProcessing;

public class MounterTest {

    private EventProcessor dataProcessing;
    private DataEvent dataEvent;
    private FactStorage factStorage;

    @Before
    public void createCoreCalculator() throws Exception {
        factStorage = new FactStorage();
        dataProcessing = mock(EventProcessor.class);
        DataEvent dataEvent1 = mock(DataEvent.class);
        DataEvent dataEvent2 = mock(DataEvent.class);
        Action action1 = mock(Action.class);
        SnapShotEvent snapShotEvent1 = mock(SnapShotEvent.class);

        when(action1.getActionValue()).thenReturn('A');
        when(dataProcessing.readNextEvent()).thenReturn(dataEvent1, dataEvent2, snapShotEvent1, null);
        when(dataEvent1.getSubject()).thenReturn("I");
        when(dataEvent1.getPredicate()).thenReturn("Am");
        when(dataEvent1.getObject()).thenReturn("Old");
        when(dataEvent1.getAction()).thenReturn(action1); //.getActionValue()
        when(dataEvent2.getSubject()).thenReturn("You");
        when(dataEvent2.getPredicate()).thenReturn("Are");
        when(dataEvent2.getObject()).thenReturn("Young");
        when(dataEvent2.getAction()).thenReturn(action1);
        when(snapShotEvent1.getName()).thenReturn("#1#");

    }

    @Test
    public void checkEventToFact() throws IOException {
        Mounter mounter = new Mounter(factStorage);
        assertEquals(mounter.getClass(), Mounter.class);
        assertNotNull(mounter);
    }

    @Test
    public void eventToFact() throws Exception {
        Mounter factSaver = new Mounter(factStorage);
        Fact fact = new Fact(0, 0, 0);
        DataEvent dataEvent = new DataEvent("I", "Am", "Old", 'A');
        Fact factResult = factSaver.translate(dataEvent);
        assertEquals(factResult.getSubject(), fact.getSubject());
        assertEquals(factResult.getPredicate(), fact.getPredicate());
        assertEquals(factResult.getObject(), fact.getObject());
    }

    @After
    public void eventToFact2() throws IOException {
        Mounter factSaver = new Mounter(factStorage);
        Fact fact = new Fact(1, 1, 1);
        Fact factResult = factSaver.translate((DataEvent) dataProcessing.readNextEvent());
        factResult = factSaver.translate((DataEvent) dataProcessing.readNextEvent());
        assertEquals(factResult.getSubject(), fact.getSubject());
        assertEquals(factResult.getPredicate(), fact.getPredicate());
        assertEquals(factResult.getObject(), fact.getObject());
    }
}