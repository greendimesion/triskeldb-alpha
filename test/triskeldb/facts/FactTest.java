package triskeldb.facts;

import org.junit.Test;
import static org.junit.Assert.*;

public class FactTest {

    @Test
    public void checkFact() {
        Fact fact = new Fact(1, 1, 1);
        assertEquals(fact.getClass(), Fact.class);
        assertNotNull(fact);
    }

    @Test
    public void Subject() {
        Fact fact = new Fact(1, 1, 1);
        assertEquals(fact.getSubject(), 1, 0);
    }

    @Test
    public void othersSubject() {
        Fact fact = new Fact(2, 1, 1);
        assertEquals(fact.getSubject(), 2, 0);
    }

    @Test
    public void Predicate() {
        Fact fact = new Fact(1, 1, 1);
        assertEquals(fact.getPredicate(), 1, 0);
    }

    @Test
    public void othersPredicate() {
        Fact fact = new Fact(1, 2, 1);
        assertEquals(fact.getPredicate(), 2, 0);
    }

    @Test
    public void Object() {
        Fact fact = new Fact(1, 1, 1);
        assertEquals(fact.getObject(), 1, 0);
    }

    @Test
    public void othersObject() {
        Fact fact = new Fact(1, 1, 2);
        assertEquals(fact.getObject(), 2, 0);
    }
}