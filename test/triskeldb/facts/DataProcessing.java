package triskeldb.facts;

public interface DataProcessing {

    public String[] readNextEvent();
}
