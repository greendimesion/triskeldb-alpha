package triskeldb.snapshooter;

import triskeldb.snapshooter.snapshot.SnapShot;
import java.io.IOException;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import triskeldb.facts.translator.Library;
import triskeldb.storage.FactStorage;

public class SnapShooterTest {

    private Library library;
    private FactStorage factsStorage;
    private SnapShooterController snapShooterController;

    public SnapShooterTest() throws IOException, Exception {
        factsStorage = new FactStorage();
        snapShooterController = new SnapShooterController(factsStorage);
    }

    @Before
    public void LoadSnapShot() throws IOException, ClassNotFoundException {
        SnapShot snapShot = null;
        assertNull(snapShot = snapShooterController.load());
        //assertNull(snapShot.getLibrary());
        //assertNull(snapShot.getFactsStorage());
    }

    @Test
    public void saveAndLoadSnapShot() throws IOException, ClassNotFoundException {
        snapShooterController.save();
        SnapShot snapShot = snapShooterController.load();
        assertNotNull(snapShot.getLibrary());
        assertNotNull(snapShot.getFactsStorage());
    }
}
