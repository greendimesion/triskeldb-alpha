package triskeldb.facts;

public class Fact {

    private final int subject;
    private final int predicate;
    private final int object;

    public Fact(int subject, int predicate, int object) {
        this.subject = subject;
        this.predicate = predicate;
        this.object = object;
    }

    public int getSubject() {
        return subject;
    }

    public int getPredicate() {
        return predicate;
    }

    public int getObject() {
        return object;
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof Fact) {
            Fact fact = (Fact) object;
            return (fact.getSubject() == getSubject()) && (fact.getObject() == getObject()) && (fact.getPredicate() == getPredicate());
        } else {
            return false;
        }
    }
}
