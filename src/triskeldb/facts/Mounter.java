package triskeldb.facts;

import java.io.IOException;
import triskeldb.events.event.DataEvent;
import triskeldb.facts.translator.Library;
import triskeldb.facts.translator.Translator;
import triskeldb.snapshooter.SnapShooterController;
import triskeldb.storage.FactStorage;

public class Mounter {

    FactStorage factStorage;
    SnapShooterController shooterController;
    Translator translator;
    Library library;

    public Mounter(FactStorage factStorage) throws IOException {
        this.factStorage = factStorage;
        //this.shooterController = new SnapShooterController();
        this.translator = Translator.getInstance();
        this.library = Library.getInstance();
    }

    public Fact translate(DataEvent event) {
        library.addSubject(event.getSubject());
        library.addPredicate(event.getPredicate());
        library.addObject(event.getObject());
        return new Fact(translator.translateSubject(event.getSubject()),
                translator.translatePredicate(event.getPredicate()),
                translator.translateObject(event.getObject()));
    }

    public void mount(DataEvent dataEvent) {
        Fact fact = translate(dataEvent);
        if (dataEvent.getAction().getActionValue() == ('A')) {
            add(fact);
        } else {
            extract(fact);
        }
    }

    private void add(Fact fact) {
        factStorage.add(fact);
    }

    private void extract(Fact fact) {
        factStorage.extract(fact);
    }
}
