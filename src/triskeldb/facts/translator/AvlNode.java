package triskeldb.facts.translator;

public class AvlNode<T extends Comparable> implements Comparable<AvlNode> {

    private T value;
    private AvlNode left;
    private AvlNode right;
    private int height = 0;

    public AvlNode(T value) {
        this(value, null, null);
    }

    public AvlNode(T value, AvlNode leftNode, AvlNode rightNode) {
        this.value = value;
        this.left = leftNode;
        this.right = rightNode;
        this.height = 0;
    }

    public AvlNode getLeft() {
        return left;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public AvlNode getRight() {
        return right;
    }

    public void setLeft(AvlNode left) {
        this.left = left;
    }

    public void setRight(AvlNode right) {
        this.right = right;
    }

    public boolean hasChilds() {
        return (left != null || right != null);
    }

    public T getValue() {
        return value;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public int compareTo(AvlNode o) {
        return this.getValue().compareTo(o.getValue());
    }
}