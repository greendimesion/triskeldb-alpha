package triskeldb.facts.translator;

import java.io.Serializable;

public class Library implements Serializable, Translatable {

    private static Library INSTANCE = new Library();
    private TranslationsDictionary<Integer, String> idToStringSubject;
    private TranslationsDictionary<String, Integer> stringToIdSubject;
    private TranslationsDictionary<Integer, String> idToStringPredicate;
    private TranslationsDictionary<String, Integer> stringToIdPredicate;
    private TranslationsDictionary<Integer, String> idToStringObject;
    private TranslationsDictionary<String, Integer> stringToIdObject;
    private Integer IDSubject;
    private Integer IDPredicate;
    private Integer IDObject;

    public Library() {
        this.IDSubject = 0;
        this.IDPredicate = 0;
        this.IDObject = 0;

        this.idToStringSubject = new TranslationsDictionary<>();
        this.stringToIdSubject = new TranslationsDictionary<>();
        this.idToStringPredicate = new TranslationsDictionary<>();
        this.stringToIdPredicate = new TranslationsDictionary<>();
        this.idToStringObject = new TranslationsDictionary<>();
        this.stringToIdObject = new TranslationsDictionary<>();
    }

    public static Library getInstance() {
        return INSTANCE;
    }

    public Integer getIDSubject() {
        return IDSubject;
    }

    public Integer getIDPredicate() {
        return IDPredicate;
    }

    public Integer getIDObject() {
        return IDObject;
    }

    public void addSubject(String string) {
        if (!stringToIdSubject.containsKey(string)) {
            idToStringSubject.put(IDSubject, string);
            stringToIdSubject.put(string, IDSubject++);
        }
    }

    public void addPredicate(String string) {
        if (!stringToIdPredicate.containsKey(string)) {
            idToStringPredicate.put(IDPredicate, string);
            stringToIdPredicate.put(string, IDPredicate++);
        }
    }

    public void addObject(String string) {
        if (!stringToIdObject.containsKey(string)) {
            idToStringObject.put(IDObject, string);
            stringToIdObject.put(string, IDObject++);
        }
    }

    @Override
    public Integer translateSubject(String string) {
        return stringToIdSubject.get(string);
    }

    @Override
    public String translateSubject(Integer id) {
        return idToStringSubject.get(id);
    }

    @Override
    public Integer translatePredicate(String string) {
        return stringToIdPredicate.get(string);
    }

    @Override
    public String translatePredicate(Integer id) {
        return idToStringPredicate.get(id);
    }

    @Override
    public Integer translateObject(String string) {
        return stringToIdObject.get(string);
    }

    @Override
    public String translateObject(Integer id) {
        return idToStringObject.get(id);
    }

    //Subjects
    public TranslationsDictionary getIdToStringSubject() {
        return idToStringSubject;
    }

    public void setIdToStringSubject(TranslationsDictionary<Integer, String> idToString) {
        idToStringSubject = idToString;
        IDSubject = idToString.size();
    }

    public TranslationsDictionary getStringToIdSubject() {
        return stringToIdSubject;
    }

    public void setStringToIdSubject(TranslationsDictionary<String, Integer> stringToId) {
        stringToIdSubject = stringToId;
        IDSubject = stringToId.size();
    }

    //Predicates
    public TranslationsDictionary getIdToStringPredicates() {
        return idToStringPredicate;
    }

    public void setIdToStringPredicates(TranslationsDictionary<Integer, String> idToString) {
        idToStringPredicate = idToString;
        IDPredicate = idToString.size();
    }

    public TranslationsDictionary getStringToIdPredicates() {
        return stringToIdPredicate;
    }

    public void setStringToIdPredicates(TranslationsDictionary<String, Integer> stringToId) {
        stringToIdPredicate = stringToId;
        IDPredicate = stringToId.size();
    }

    //Objects
    public TranslationsDictionary getIdToStringObjects() {
        return idToStringObject;
    }

    public void setIdToStringObjects(TranslationsDictionary<Integer, String> idToString) {
        idToStringObject = idToString;
        IDObject = idToString.size();
    }

    public TranslationsDictionary getStringToIdObjects() {
        return stringToIdObject;
    }

    public void setStringToIdObjects(TranslationsDictionary<String, Integer> stringToId) {
        stringToIdObject = stringToId;
        IDObject = stringToId.size();
    }
}
