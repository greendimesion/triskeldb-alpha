package triskeldb.facts.translator;

public interface Translatable {

    public Integer translateSubject(String string);

    public String translateSubject(Integer id);

    public Integer translatePredicate(String string);

    public String translatePredicate(Integer id);

    public Integer translateObject(String string);

    public String translateObject(Integer id);
}
