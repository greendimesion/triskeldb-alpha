package triskeldb;

import triskeldb.events.eventsgenerator.EventsGenerator;
import triskeldb.maincontroller.MainController;

public class Triskeldb {

    private static final int numberOfEvents = 10;

    public static void main(String[] args) throws Exception {
        EventsGenerator eventsGenerator = new EventsGenerator();
        eventsGenerator.generate(numberOfEvents);
        MainController mainController = MainController.getInstance();

    }
}
