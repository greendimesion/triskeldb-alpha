package triskeldb.snapshooter.snapshot;

import java.io.Serializable;
import triskeldb.facts.translator.Library;
import triskeldb.storage.Storage;

public class SnapShot implements Serializable {

    private Library library;
    private Storage factsStorage;

    public SnapShot(Library library, Storage factsStorage) {
        this.library = library;
        this.factsStorage = factsStorage;
    }

    public SnapShot() {
        this.library = null;
        this.factsStorage = null;
    }

    public Library getLibrary() {
        return library;
    }

    public Storage getFactsStorage() {
        return factsStorage;
    }
}
