package triskeldb.snapshooter.snapshot;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SnapShotStorage {

    private static File file;

    public SnapShotStorage() throws IOException {
        //file = new File("C:" + File.separator + "Users" + File.separator + "user3store" + File.separator + "Desktop" + File.separator + "snapshot.dat");
        file = new File("snapshot.dat");
        if (!file.exists()) {
            file.createNewFile();
        }
    }

    public SnapShot read() throws FileNotFoundException, IOException, ClassNotFoundException {
        Object readed = null;
        FileInputStream fileInputStream = new FileInputStream(file);
        if (file.length() > 0) {
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            readed = objectInputStream.readObject();
        }
        return (readed == null) ? null : (SnapShot) readed;
    }

    public void write(SnapShot snapShot) throws FileNotFoundException, IOException {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file));
        objectOutputStream.writeObject(snapShot);
    }
}
