package triskeldb.snapshooter;

import triskeldb.snapshooter.snapshot.SnapShotStorage;
import triskeldb.snapshooter.snapshot.SnapShot;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import triskeldb.facts.translator.Library;
import triskeldb.storage.FactStorage;

public class SnapShooterController {

    private SnapShotStorage snapShotStorage;
    private FactStorage factStorage;
    private Library library;

    public SnapShooterController(FactStorage factStorage) {
        try {
            this.snapShotStorage = new SnapShotStorage();
            this.factStorage = factStorage;
            this.library = Library.getInstance();
        } catch (IOException ex) {
            Logger.getLogger(SnapShooterController.class.getName()).log(Level.SEVERE, null, ex);
            Runtime.getRuntime().exit(0);
        }
    }

    public SnapShot load() {
        SnapShot snapShot = null;
        try {
            snapShot = snapShotStorage.read();
        } catch (IOException | ClassNotFoundException exception) {
            Logger.getLogger(SnapShooterController.class.getName()).log(Level.SEVERE, null, exception);
            Runtime.getRuntime().exit(0);
        } finally {
            return snapShot;
        }

    }

    public void save() {

        try {
            snapShotStorage.write(new SnapShot(library, factStorage.getStorageState()));
        } catch (Exception exception) {
            Logger.getLogger(SnapShooterController.class.getName()).log(Level.SEVERE, null, exception);
            Runtime.getRuntime().exit(0);
        }
    }
}
