package triskeldb.querymanager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import triskeldb.facts.Fact;
import triskeldb.queries.query.PlainQuery;
import triskeldb.queries.query.Query;
import triskeldb.queries.querytranslator.QueryTranslator;
import triskeldb.storage.FactStorage;

public class QueryManager {

    private char answers;
    private ArrayList<String> subjectList;
    private ArrayList<String> predicateList;
    private ArrayList<String> objectList;
    private BufferedReader buffer;
    private FactStorage factStorage;

    public QueryManager(FactStorage factStorage) {
        this.buffer = new BufferedReader(new InputStreamReader(System.in));
        this.subjectList = new ArrayList<>();
        this.predicateList = new ArrayList<>();
        this.objectList = new ArrayList<>();
        this.factStorage = factStorage;
    }

    public void runQueryManager() {
        try {
            askForQuery();
            String[] subject = subjectListToStringArray();
            String[] predicate = predicateListToStringArray();
            String[] object = objectListToStringArray();
            evaluateQuery(subject, predicate, object);
        } catch (IOException ex) {
            System.out.println("Invalid Execution: " + ex.getMessage());
        }
    }

    private void askForSubjectList() throws IOException {
        do {
            System.out.println("Introduces Subject : ");
            subjectList.add(buffer.readLine());
            System.out.println("Do you want introduce a new Subject? Press 'N' or 'n' to finish");
            answers = buffer.readLine().charAt(0);
        } while (answers != 'N' && answers != 'n');
    }

    private void askForPredicateList() throws IOException {
        do {
            System.out.println("Introduces Predicate : ");
            predicateList.add(buffer.readLine());
            System.out.println("Do you want introduce a new Predicate? Press 'N' or 'n' to finish");
            answers = buffer.readLine().charAt(0);
        } while (answers != 'N' && answers != 'n');
    }

    private void askForObjectList() throws IOException {
        do {
            System.out.println("Introduces Object : ");
            objectList.add(buffer.readLine());
            System.out.println("Do you want introduce a new Object? Press 'N' or 'n' to finish");
            answers = buffer.readLine().charAt(0);
        } while (answers != 'N' && answers != 'n');
    }

    private String[] subjectListToStringArray() {
        String[] subject = subjectList.toArray(new String[subjectList.size()]);
        return subject;
    }

    private String[] predicateListToStringArray() {
        String[] predicate = predicateList.toArray(new String[predicateList.size()]);
        return predicate;
    }

    private String[] objectListToStringArray() {
        String[] object = objectList.toArray(new String[objectList.size()]);
        return object;
    }

    private void askForQuery() throws IOException {
        try {
            askForSubjectList();
            askForPredicateList();
            askForObjectList();
        } catch (Exception exception) {
            Logger logger = Logger.getLogger("IOException");
            logger.log(Level.SEVERE, " " + exception.getMessage());
            Runtime.getRuntime().exit(0);
        }
    }

    private void evaluateQuery(String[] subject, String[] predicate, String[] object) {
        Query firstQuery = new Query(subject, predicate, object);
        QueryTranslator queryTranslator = new QueryTranslator();
        evaluateAllPlainQueries(firstQuery, queryTranslator);
    }

    private void showComplexQueryResult(QueryTranslator queryTranslator, ArrayList<Fact> validFacts) {
        ArrayList<PlainQuery> plainQueryList;
        plainQueryList = queryTranslator.translateIdQueries(validFacts);
        for (PlainQuery actualQuery : plainQueryList) {
            System.out.println(actualQuery.getSubject() + " " + actualQuery.getPredicate() + " " + actualQuery.getObject());
        }
    }

    private ArrayList<Fact> checkFactsOnCube(QueryTranslator queryTranslator, PlainQuery plainQuery) {
        ArrayList<Fact> factList;
        ArrayList<Fact> validFacts = new ArrayList<>();
        factList = queryTranslator.complexTranslate(plainQuery);
        for (Fact actualFact : factList) {
            if (factStorage.matchPattern(actualFact.getSubject(), actualFact.getPredicate(), actualFact.getObject())) {
                validFacts.add(actualFact);
            }
        }
        return validFacts;
    }

    private void showSimpleQueryResult(QueryTranslator queryTranslator, PlainQuery plainQuery) {
        try {
            Fact fact = queryTranslator.simpleTranslate(plainQuery);
            System.out.println(factStorage.matchPattern(fact.getSubject(), fact.getPredicate(), fact.getObject()));
        } catch (Exception exception) {
            System.out.println("False");
        }
    }

    private void evaluateAllPlainQueries(Query firstQuery, QueryTranslator queryTranslator) {
        PlainQuery plainQuery;
        while (firstQuery.areThereMoreQueries()) {
            plainQuery = firstQuery.nextPlainQuery();
            if (!plainQuery.isComplex()) {
                showSimpleQueryResult(queryTranslator, plainQuery);
            } else {
                ArrayList<Fact> validFacts = checkFactsOnCube(queryTranslator, plainQuery);
                showComplexQueryResult(queryTranslator, validFacts);
            }
        }
    }
}
