package triskeldb.events.dataprocessing;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import triskeldb.events.event.Event;
import triskeldb.events.event.SnapShotEvent;

public class EventProcessor {

    private static EventProcessor eventProcessor = null;
    private File file;
    private long totalElements;
    private long lastSnapshot;
    private ObjectOutputStream objectOutputStream;
    private static FIFOList list = new FIFOList();

    private EventProcessor() {
        file = new File("C:" + File.separator + "Users" + File.separator
                + "user3store" + File.separator + "Desktop" + File.separator
                + "Events.dat");
        //file = new File("."+File.separator+"triskeldb"+File.separator+"events"+File.separator+"dataprocessing"+File.separator+"tripletas.txt");
        totalElements = 0;
        lastSnapshot = 0;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public static synchronized EventProcessor getInstance() {
        return (eventProcessor == null) ? new EventProcessor() : eventProcessor;
    }

    public Event readNextEvent() {
        return list.extractNextEvent();
    }

    public void add(Event event) {
        writeInFile(event);
        totalElements++;
        if (IsSnapShot(event)) {
            lastSnapshot = CalculatePositionSnapShot();
        } else {
            list.addEvent(event);
        }
    }

    private long getSize() {
        return file.length();
    }

    private void writeInFile(Event event) {
        try {
            objectOutputStream = new ObjectOutputStream(new FileOutputStream(file));
            objectOutputStream.writeObject(event);
            objectOutputStream.close();
        } catch (IOException ex) {
            Logger.getLogger(EventProcessor.class.getName()).log(Level.SEVERE, null, ex);
            Runtime.getRuntime().exit(0);
        }
    }

    private boolean IsSnapShot(Event event) {
        return event instanceof SnapShotEvent;
    }

    private long CalculatePositionSnapShot() {
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
            randomAccessFile.seek(this.getSize());
            return randomAccessFile.getFilePointer() - 1;
        } catch (IOException ex) {
            Logger.getLogger(EventProcessor.class.getName()).log(Level.SEVERE, null, ex);
            Runtime.getRuntime().exit(0);
        }
        return -1;
    }
}