package triskeldb.events.dataprocessing;

import triskeldb.events.event.Event;

public class FIFOList {

    private Nodo first = null;
    private Nodo last = null;

    private static class Nodo {

        Event event;
        Nodo next;
    }

    public void addEvent(Event event) {
        Nodo newEventValue = new Nodo();
        newEventValue.event = event;
        newEventValue.next = null;
        if (first == null) {
            first = newEventValue;
            last = newEventValue;
        } else {
            last.next = newEventValue;
            last = newEventValue;
        }
    }

    public Event extractNextEvent() {
        if (first != null) {
            Event event = first.event;
            first = first.next;
            return event;
        }
        if (last != null) {
            last = null;
        }
        return null;
    }
}
