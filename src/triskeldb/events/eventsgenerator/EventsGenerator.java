package triskeldb.events.eventsgenerator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import triskeldb.events.dataprocessing.EventProcessor;
import triskeldb.events.event.DataEvent;

public class EventsGenerator {

    private Integer limite = 50;
    EventProcessor eventProcessor = null;
    private File fileSubjects;
    private File filePredicates;
    private File fileObjects;
    private Integer eventsgenerated = 0;
    private BufferedReader bufferedReader;
    private FileReader fileReader;

    public EventsGenerator() {
        this.eventProcessor = EventProcessor.getInstance();

        fileSubjects = new File("Subjects.txt");
        filePredicates = new File("Predicates.txt");
        fileObjects = new File("Objects.txt");
    }

    public int eventsGenerated() {
        return eventsgenerated;
    }

    public void generate(int eventsNumber) {

        try {
            eventProcessor = EventProcessor.getInstance();
            int objectToRead;
            for (int i = 0; i < eventsNumber; i++) {
                openFile(fileSubjects);
                objectToRead = randomNumber();
                String subject = selectEventComponent(objectToRead, fileSubjects);
                fileReader.close();

                openFile(filePredicates);
                objectToRead = randomNumber();
                String predicate = selectEventComponent(objectToRead, filePredicates);
                fileReader.close();

                openFile(fileObjects);
                objectToRead = randomNumber();
                String object = selectEventComponent(objectToRead, fileObjects);
                fileReader.close();

                char action = getRandomAction();
                eventProcessor.add(new DataEvent(subject, predicate, object, action));
                eventsgenerated++;
                System.out.println(subject + "-" + predicate + "-" + object);
            }
        } catch (Exception exception) { // IOException FileNotFoundException
            Logger.getLogger(EventsGenerator.class.getName()).log(Level.SEVERE, null, exception);
            Runtime.getRuntime().exit(0);
        } finally {
            try {
                fileReader.close();
            } catch (Exception exception) {
                Logger.getLogger(EventsGenerator.class.getName()).log(Level.SEVERE, null, exception);
                Runtime.getRuntime().exit(0);
            }
        }

    }

    private int randomNumber() {
        return ((int) ((Math.random() * limite) + 1));
    }

    private void openFile(File nameFile) throws FileNotFoundException {
        fileReader = new FileReader(nameFile);
        bufferedReader = new BufferedReader(fileReader);
    }

    private String selectEventComponent(int objectToRead, File file) throws IOException {
        String lineOfFile = null;
        for (int j = 0; j < objectToRead; j++) {
            lineOfFile = bufferedReader.readLine();
            if (lineOfFile == null) {
                fileReader.close();
                openFile(file);
                lineOfFile = bufferedReader.readLine();
            }
        }
        return lineOfFile;
    }

    private char getRandomAction() {
        char[] actions = {'A', 'E'};
        return actions[(int) ((Math.random() + 1)) - 1];
    }
}
