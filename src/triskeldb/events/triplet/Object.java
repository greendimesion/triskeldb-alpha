package triskeldb.events.triplet;

import java.io.Serializable;

public class Object implements Serializable {

    private String object;

    public Object(String object) {
        this.object = object;
    }

    public String getObject() {
        return object;
    }
}
