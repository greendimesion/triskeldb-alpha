package triskeldb.events.triplet;

import java.io.Serializable;

public class Triplet implements Serializable {

    private Subject subject;
    private Predicate predicate;
    private Object objtect;

    public Triplet(Subject subject, Predicate predicate, Object object) {
        InitializeSPO(subject, predicate, object);
    }

    public Triplet(String subject, String predicate, String object) {
        Subject s = new Subject(subject);
        Predicate p = new Predicate(predicate);
        Object o = new Object(object);
        InitializeSPO(s, p, o);
    }

    public Subject getSubject() {
        return subject;
    }

    public Predicate getPredicate() {
        return predicate;
    }

    public Object getObject() {
        return objtect;
    }

    public String getSubjectValue() {
        return subject.getSubject();
    }

    public String getPredicateValue() {
        return predicate.getPredicate();
    }

    public String getObjectValue() {
        return objtect.getObject();
    }

    private void InitializeSPO(Subject subject, Predicate predicate, Object object) {
        this.subject = subject;
        this.predicate = predicate;
        this.objtect = object;
    }
}
