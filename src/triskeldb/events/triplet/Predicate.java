package triskeldb.events.triplet;

import java.io.Serializable;

public class Predicate implements Serializable {

    private String predicate;

    public Predicate(String predicate) {
        this.predicate = predicate;
    }

    public String getPredicate() {
        return predicate;
    }
}
