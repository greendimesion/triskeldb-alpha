package triskeldb.events.triplet;

import java.io.Serializable;

public class Subject implements Serializable {

    private String subject;

    public Subject(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return this.subject;
    }
}
