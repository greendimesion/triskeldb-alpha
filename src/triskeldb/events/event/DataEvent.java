package triskeldb.events.event;

import triskeldb.events.actions.Action;
import triskeldb.events.timestamp.TimeStamp;
import triskeldb.events.triplet.Triplet;

public class DataEvent extends Event {

    private Triplet triplet;
    private Action action;
    private TimeStamp timeStamp;

    public DataEvent(String subject, String predicate, String object, char action) {
        triplet = new Triplet(subject, predicate, object);
        this.action = new Action(action);
        timeStamp = new TimeStamp();
    }

    public Triplet getTriplet() {
        return triplet;
    }

    public String getSubject() {
        return triplet.getSubjectValue();
    }

    public String getPredicate() {
        return triplet.getPredicateValue();
    }

    public String getObject() {
        return triplet.getObjectValue();
    }

    public Action getAction() {
        return action;
    }

    public TimeStamp getTimeStamp() {
        return timeStamp;
    }

    public String[] getEventValue() {
        String[] eventDatas = {
            triplet.getSubjectValue(),
            triplet.getPredicateValue(),
            triplet.getObjectValue(),
            String.valueOf(action.getActionValue()),
            String.valueOf(timeStamp.getSystemNumber())
        };
        return eventDatas;
    }
}
