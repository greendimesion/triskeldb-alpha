package triskeldb.events.event;

public class SnapShotEvent extends Event {

    private String name;

    public SnapShotEvent() {
        this.name = "#1#";
    }

    public String getName() {
        return name;
    }
}
