package triskeldb.maincontroller;

import java.util.logging.Level;
import java.util.logging.Logger;
import triskeldb.events.dataprocessing.EventProcessor;
import triskeldb.events.event.DataEvent;
import triskeldb.events.event.Event;
import triskeldb.events.event.SnapShotEvent;
import triskeldb.facts.Mounter;
//import triskeldb.facts.translator.Translator;
import triskeldb.querymanager.QueryManager;
import triskeldb.snapshooter.SnapShooterController;
import triskeldb.storage.FactStorage;

public class MainController extends Thread {

    private static MainController mainController = null;
    private EventProcessor eventProcessor;
    private FactStorage factStorage;
    private Mounter mounter;
    private QueryManager queryManager;
    //private Translator translator;
    private SnapShooterController snapShooterController;
    private Boolean stopped;
    private SnapShotEvent snapShotEvent;

    public MainController() {
        try {
            factStorage = new FactStorage();
            mounter = new Mounter(factStorage);
            queryManager = new QueryManager(factStorage);
            eventProcessor = EventProcessor.getInstance();
            //translator = Translator.getInstance();
            snapShooterController = new SnapShooterController(factStorage);
            stopped = false;
            init();
        } catch (Exception exception) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, exception);
        }
    }

    public static synchronized MainController getInstance() {
        return (mainController == null) ? new MainController() : mainController;
    }

    @Override
    public void run() {
        Event event = eventProcessor.readNextEvent();
        Integer countProcecedEvents = 0;
        while (!stopped) {
            try {
                if (event != null) {
                    countProcecedEvents++;
                    DataEvent dataEvent = (DataEvent) event;
                    mounter.mount(dataEvent);
                    event = eventProcessor.readNextEvent();
                } else {
                    stopped = true;
                }
                //Comprobar que existen X eventos nuevos para realizar una foto
                if (countProcecedEvents >= 5) {
                    //Detener la inserción de eventos en la lista y en fichero.
                    //Realiza marca en el fichero de snapshot
                    snapShotEvent = new SnapShotEvent();
                    eventProcessor.add(snapShotEvent);
                    //Guarda la foto en disco
                    snapShooterController.save();
                    countProcecedEvents = 0;
                }
                sleep(40);
            } catch (InterruptedException ex) {
                System.out.println("Main Controller has been error in run function");
                Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        queryManager.runQueryManager();
    }

    private void init() {
        start();
    }
}
