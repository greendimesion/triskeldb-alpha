package triskeldb.queries.querytranslator;

import java.util.ArrayList;
import triskeldb.facts.Fact;
import triskeldb.facts.translator.Library;
import triskeldb.facts.translator.Translatable;
import triskeldb.queries.query.PlainQuery;
import triskeldb.facts.translator.Translator;

public class QueryTranslator {

    private final Translatable translator;
    private final Library library;

    public QueryTranslator() {
        this.translator = Translator.getInstance();
        this.library = Library.getInstance();
    }

    public Fact simpleTranslate(PlainQuery simpleQuery) throws NullPointerException {
        int subject = translator.translateSubject(simpleQuery.getSubject());
        int predicate = translator.translatePredicate(simpleQuery.getPredicate());
        int object = translator.translateObject(simpleQuery.getObject());
        Fact factResult = new Fact(subject, predicate, object);
        return factResult;
    }

    public ArrayList<Fact> complexTranslate(PlainQuery complexQuery) {
        ArrayList<Fact> factList = new ArrayList<>();
        int iterator = 0;
        if (complexQuery.getSubject().equals("?")) {
            if ((complexQuery.getPredicate() == "?") || (complexQuery.getObject() == "?")) {
                while (iterator < library.getIDSubject()) {
                    factList.addAll(complexTranslate(new PlainQuery(translator.translateSubject(iterator++),
                            complexQuery.getPredicate(), complexQuery.getObject())));
                }
            } else {
                int predicate = translator.translatePredicate(complexQuery.getPredicate());
                int object = translator.translateObject(complexQuery.getObject());
                while (iterator < library.getIDSubject()) {
                    factList.add(new Fact(iterator++, predicate, object));
                }
            }
            return factList;
        }
        if (complexQuery.getPredicate().equals("?")) {
            if (complexQuery.getObject() == "?") {
                while (iterator < library.getIDPredicate()) {
                    factList.addAll(complexTranslate(new PlainQuery(complexQuery.getSubject(),
                            translator.translatePredicate(iterator++), complexQuery.getObject())));
                }
            } else {
                int subject = translator.translateSubject(complexQuery.getSubject());
                int object = translator.translateObject(complexQuery.getObject());
                while (iterator < library.getIDPredicate()) {
                    factList.add(new Fact(subject, iterator++, object));
                }
            }
            return factList;
        }
        if (complexQuery.getObject().equals("?")) {
            int subject = translator.translateSubject(complexQuery.getSubject());
            int predicate = translator.translatePredicate(complexQuery.getPredicate());
            while (iterator < library.getIDObject()) {
                factList.add(new Fact(subject, predicate, iterator++));
            }
        }
        return factList;
    }

    public ArrayList<PlainQuery> translateIdQueries(ArrayList<Fact> factsList) {
        ArrayList<PlainQuery> plainQueryList = new ArrayList<>();
        for (Fact actualFact : factsList) {
            plainQueryList.add(new PlainQuery(translator.translateSubject(actualFact.getSubject()),
                    translator.translatePredicate(actualFact.getPredicate()),
                    translator.translateObject(actualFact.getObject())));
        }
        return plainQueryList;
    }
}
