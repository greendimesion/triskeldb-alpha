package triskeldb.queries.operation;

import java.util.ArrayList;
import triskeldb.facts.Fact;
import triskeldb.queries.query.PlainQuery;
import triskeldb.queries.querytranslator.QueryTranslator;
import triskeldb.storage.MatchingManager;

public class Operation {

    MatchingManager patternMatcher;
    QueryTranslator translator;

    public Operation(MatchingManager patternMatcher) {
        this.patternMatcher = patternMatcher;
        this.translator = new QueryTranslator();
    }

    public ArrayList<PlainQuery> union(PlainQuery firstQuery, PlainQuery secondQuery) {
        ArrayList<Fact> unionResultInFacts = new ArrayList<>();
        if (!firstQuery.isComplex()) {
            Fact firstFact = translator.simpleTranslate(firstQuery);
            if (patternMatcher.matchPattern(firstFact.getSubject(), firstFact.getPredicate(), firstFact.getObject())) {
                unionResultInFacts.add(firstFact);
            }
        } else {
            ArrayList<Fact> firstFactList = translator.complexTranslate(firstQuery);
            for (Fact actualFact : firstFactList) {
                if (patternMatcher.matchPattern(actualFact.getSubject(), actualFact.getPredicate(), actualFact.getObject())) {
                    unionResultInFacts.add(actualFact);
                }
            }
        }
        if (!secondQuery.isComplex()) {
            Fact secondFact = translator.simpleTranslate(secondQuery);
            if (patternMatcher.matchPattern(secondFact.getSubject(), secondFact.getPredicate(), secondFact.getObject())) {
                unionResultInFacts.add(secondFact);
            }
        } else {
            ArrayList<Fact> secondFactList = translator.complexTranslate(secondQuery);
            for (Fact actualFact : secondFactList) {
                if (patternMatcher.matchPattern(actualFact.getSubject(), actualFact.getPredicate(), actualFact.getObject())) {
                    unionResultInFacts.add(actualFact);
                }
            }
        }
        return translator.translateIdQueries(unionResultInFacts);
    }

    public ArrayList<Integer> proyectionSubject(ArrayList<Fact> factList) {
        ArrayList<Integer> subjectList = new ArrayList<>();
        for (Fact actualFact : factList) {
            subjectList.add(actualFact.getSubject());
        }
        return subjectList;
    }

    public ArrayList<Integer> proyectionPredicate(ArrayList<Fact> factList) {
        ArrayList<Integer> predicateList = new ArrayList<>();
        for (Fact actualFact : factList) {
            predicateList.add(actualFact.getPredicate());
        }
        return predicateList;
    }

    public ArrayList<Integer> proyectionObject(ArrayList<Fact> factList) {
        ArrayList<Integer> objectList = new ArrayList<>();
        for (Fact actualFact : factList) {
            objectList.add(actualFact.getPredicate());
        }
        return objectList;
    }

    public ArrayList<PlainQuery> intersection(PlainQuery firstQuery, PlainQuery secondQuery) {
        ArrayList<Fact> intersectionResult = new ArrayList<>();
        if (!firstQuery.isComplex()) {
            if (firstQuery.equals(secondQuery)
                    && patternMatcher.matchPattern(
                    translator.simpleTranslate(firstQuery).getSubject(),
                    translator.simpleTranslate(firstQuery).getPredicate(),
                    translator.simpleTranslate(firstQuery).getObject())) {
                intersectionResult.add(translator.simpleTranslate(firstQuery));
            } else {
                if (secondQuery.isComplex()) {
                    Fact firstQueryFact = translator.simpleTranslate(firstQuery);
                    ArrayList<Fact> secondQueryFactList = translator.complexTranslate(secondQuery);
                    for (Fact actualFactSecondList : secondQueryFactList) {
                        if (firstQueryFact.equals(actualFactSecondList)) {
                            if (patternMatcher.matchPattern(firstQueryFact.getSubject(), firstQueryFact.getPredicate(), firstQueryFact.getObject())) {
                                intersectionResult.add(firstQueryFact);
                            }
                        }
                    }
                }
            }
        } else {
            ArrayList<Fact> firstQueryFactList = translator.complexTranslate(firstQuery);
            if (firstQuery.equals(secondQuery)) {
                for (Fact actualFactFirstList : firstQueryFactList) {
                    if (patternMatcher.matchPattern(actualFactFirstList.getSubject(), actualFactFirstList.getPredicate(), actualFactFirstList.getObject())) {
                        intersectionResult.add(actualFactFirstList);
                    }
                }
            } else {
                if (secondQuery.isComplex()) {
                    ArrayList<Fact> secondQueryFactList = translator.complexTranslate(secondQuery);
                    for (Fact actualFactFirstList : firstQueryFactList) {
                        for (Fact actualFactSecondList : secondQueryFactList) {
                            if (actualFactFirstList.equals(actualFactSecondList)) {
                                if (patternMatcher.matchPattern(actualFactFirstList.getSubject(), actualFactFirstList.getPredicate(), actualFactFirstList.getObject())) {
                                    intersectionResult.add(actualFactFirstList);
                                }
                            }
                        }
                    }
                } else {
                    Fact secondQueryFact = translator.simpleTranslate(secondQuery);
                    for (Fact actualFactFirstList : firstQueryFactList) {
                        if (secondQueryFact.equals(actualFactFirstList)) {
                            if (patternMatcher.matchPattern(actualFactFirstList.getSubject(), actualFactFirstList.getPredicate(), actualFactFirstList.getObject())) {
                                intersectionResult.add(actualFactFirstList);
                            }
                        }
                    }
                }
            }
        }
        return translator.translateIdQueries(intersectionResult);
    }
}
