package triskeldb.queries.query;

public class TwoElementsTuple {

    private int firstValue;
    private int secondValue;

    public TwoElementsTuple(int firstValue, int secondValue) {
        this.firstValue = firstValue;
        this.secondValue = secondValue;
    }

    public int getFirstValue() {
        return firstValue;
    }

    public int getSecondValue() {
        return secondValue;
    }
}
