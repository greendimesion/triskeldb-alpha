package triskeldb.queries.query;

public class PlainQuery {

    private String subject;
    private String predicate;
    private String object;
    private boolean complex;

    public PlainQuery(String subject, String predicate, String object) {
        this.subject = subject;
        this.predicate = predicate;
        this.object = object;
        this.complex = evaluateComplexity();
    }

    public String getSubject() {
        return subject;
    }

    public String getPredicate() {
        return predicate;
    }

    public String getObject() {
        return object;
    }

    public boolean isComplex() {
        return this.complex;
    }

    private boolean evaluateComplexity() {
        return subject.equals("?") || predicate.equals("?") || object.equals("?");
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof PlainQuery) {
            PlainQuery plainQuery = (PlainQuery) object;
            return (plainQuery.getSubject() == getSubject()) && (plainQuery.getObject() == getObject()) && (plainQuery.getPredicate() == getPredicate());
        } else {
            return false;
        }
    }
}
