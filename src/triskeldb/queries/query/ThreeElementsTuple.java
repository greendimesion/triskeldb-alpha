package triskeldb.queries.query;

public class ThreeElementsTuple {

    private int firstValue;
    private int secondValue;
    private int thirdValue;

    public ThreeElementsTuple(int firstValue, int secondValue, int thirdValue) {
        this.firstValue = firstValue;
        this.secondValue = secondValue;
        this.thirdValue = thirdValue;
    }

    public int getFirstValue() {
        return firstValue;
    }

    public int getSecondValue() {
        return secondValue;
    }

    public int getThirdValue() {
        return thirdValue;
    }
}
