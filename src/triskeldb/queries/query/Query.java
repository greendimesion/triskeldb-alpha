package triskeldb.queries.query;

import java.util.ArrayList;

public final class Query {

    private ArrayList<PlainQuery> queryList;
    private int actualQuery;

    public Query(String[] subjectTuple, String[] predicateTuple, String[] objectTuple) {
        this.queryList = new ArrayList<>();
        this.actualQuery = 0;
        generatePlainQueries(subjectTuple, predicateTuple, objectTuple);
    }

    public void generatePlainQueries(String[] subjectTuple, String[] predicateTuple, String[] objectTuple) {
        for (String actualSubject : subjectTuple) {
            for (String actualPredicate : predicateTuple) {
                for (String actualObject : objectTuple) {
                    this.queryList.add(new PlainQuery(actualSubject, actualPredicate, actualObject));
                }
            }
        }
    }

    public PlainQuery nextPlainQuery() {
        PlainQuery simpleQuery = queryList.get(actualQuery);
        actualQuery++;
        return simpleQuery;
    }

    public boolean areThereMoreQueries() {
        return !(this.actualQuery == queryList.size());
    }
}
