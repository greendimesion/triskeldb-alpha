package triskeldb.storage;

import java.io.Serializable;
import triskeldb.facts.Fact;

public interface StorageManager extends Serializable {

    public void add(Fact fact);

    public void extract(Fact fact);

    public Storage getStorageState();

    public void setStorageState(Storage newStorage);
}
