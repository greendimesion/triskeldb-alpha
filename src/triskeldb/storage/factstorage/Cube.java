package triskeldb.storage.factstorage;

import java.util.ArrayList;
import triskeldb.queries.query.ThreeElementsTuple;
import triskeldb.queries.query.TwoElementsTuple;
import triskeldb.facts.Fact;
import triskeldb.storage.Storage;

public class Cube implements Storage {

    private byte[][][] cube;

    public Cube(int Subjects, int Predicates, int Objects) throws Exception {
        if (Subjects == 0 || Predicates == 0 || Objects == 0) {
            throw new Exception("Cube size not allowed");
        }
        cube = new byte[Subjects][Predicates][Objects];
        init();
    }

    private void init() {
        for (int i = 0; i < getNumberOfSubjects(); i++) {
            for (int j = 0; j < getNumberOfPredicates(); j++) {
                for (int k = 0; k < getNumberOfObjects(); k++) {
                    cube[i][j][k] = 0;
                }
            }
        }
    }

    public int getNumberOfSubjects() {
        return cube.length;
    }

    public int getNumberOfPredicates() {
        return cube[0].length;
    }

    public int getNumberOfObjects() {
        return cube[0][0].length;
    }

    @Override
    public boolean matchPattern(Integer subject, Integer predicate, Integer object) {
        return cube[subject][predicate][object] == 3;
    }

    @Override
    public void add(Fact fact) throws IndexOutOfBoundsException {
        cube[fact.getSubject()][fact.getPredicate()][fact.getObject()] = 3;
    }

    @Override
    public void extract(Fact fact) throws IndexOutOfBoundsException {
        cube[fact.getSubject()][fact.getPredicate()][fact.getObject()] = 1;
    }

    @Override
    public ArrayList<Integer> matchParcialSubject(Integer predicate, Integer object) {
        ArrayList<Integer> subjectFound = new ArrayList<>();
        for (int subjectIndex = 0; subjectIndex < getNumberOfSubjects(); subjectIndex++) {
            if (matchPattern(subjectIndex, predicate, object)) {
                subjectFound.add(subjectIndex);
            }
        }
        return subjectFound;
    }

    @Override
    public ArrayList<Integer> matchParcialPredicate(Integer subject, Integer object) {
        ArrayList<Integer> predicateFound = new ArrayList<>();
        for (int predicateIndex = 0; predicateIndex < getNumberOfPredicates(); predicateIndex++) {
            if (matchPattern(subject, predicateIndex, object)) {
                predicateFound.add(predicateIndex);
            }
        }
        return predicateFound;
    }

    @Override
    public ArrayList<Integer> matchParcialObject(Integer subject, Integer predicate) {
        ArrayList<Integer> objectFound = new ArrayList<>();
        for (int objectIndex = 0; objectIndex < getNumberOfObjects(); objectIndex++) {
            if (matchPattern(subject, predicate, objectIndex)) {
                objectFound.add(objectIndex);
            }
        }
        return objectFound;
    }

    @Override
    public ArrayList<TwoElementsTuple> matchParcialPredicateObject(Integer subject) {
        ArrayList<TwoElementsTuple> resultList = new ArrayList<>();
        for (int predicateIndex = 0; predicateIndex < getNumberOfPredicates(); predicateIndex++) {
            for (int objectIndex = 0; objectIndex < getNumberOfObjects(); objectIndex++) {
                if (matchPattern(subject, predicateIndex, objectIndex)) {
                    resultList.add(new TwoElementsTuple(predicateIndex, objectIndex));
                }
            }
        }
        return resultList;
    }

    @Override
    public ArrayList<TwoElementsTuple> matchParcialSubjectObject(Integer predicate) {
        ArrayList<TwoElementsTuple> resultList = new ArrayList<>();
        for (int subjectIndex = 0; subjectIndex < getNumberOfSubjects(); subjectIndex++) {
            for (int objectIndex = 0; objectIndex < getNumberOfObjects(); objectIndex++) {
                if (matchPattern(subjectIndex, predicate, objectIndex)) {
                    resultList.add(new TwoElementsTuple(subjectIndex, objectIndex));
                }
            }
        }
        return resultList;
    }

    @Override
    public ArrayList<TwoElementsTuple> matchParcialSubjectPredicate(Integer object) {
        ArrayList<TwoElementsTuple> resultList = new ArrayList<>();
        for (int subjectIndex = 0; subjectIndex < getNumberOfSubjects(); subjectIndex++) {
            for (int predicateIndex = 0; predicateIndex < getNumberOfPredicates(); predicateIndex++) {
                if (matchPattern(subjectIndex, predicateIndex, object)) {
                    resultList.add(new TwoElementsTuple(subjectIndex, predicateIndex));
                }
            }
        }
        return resultList;
    }

    @Override
    public ArrayList<ThreeElementsTuple> matchTotal() {
        ArrayList<ThreeElementsTuple> resultList = new ArrayList<>();
        for (int subjectIndex = 0; subjectIndex < getNumberOfSubjects(); subjectIndex++) {
            for (int predicateIndex = 0; predicateIndex < getNumberOfPredicates(); predicateIndex++) {
                for (int objectIndex = 0; objectIndex < getNumberOfObjects(); objectIndex++) {
                    if (matchPattern(subjectIndex, predicateIndex, objectIndex)) {
                        resultList.add(new ThreeElementsTuple(subjectIndex, predicateIndex, objectIndex));
                    }
                }
            }
        }
        return resultList;
    }
}
