package triskeldb.storage;

import java.io.Serializable;
import java.util.ArrayList;
import triskeldb.queries.query.ThreeElementsTuple;
import triskeldb.queries.query.TwoElementsTuple;

public interface MatchingManager extends Serializable {

    public boolean matchPattern(Integer subject, Integer predicate, Integer object);

    public ArrayList<Integer> matchParcialSubject(Integer predicate, Integer object);

    public ArrayList<Integer> matchParcialSubject(Integer predicate, ArrayList<Integer> object);

    public ArrayList<Integer> matchParcialPredicate(Integer subject, Integer object);

    public ArrayList<Integer> matchParcialObject(Integer subject, Integer predicate);

    public ArrayList<TwoElementsTuple> matchParcialPredicateObject(Integer subject);

    public ArrayList<TwoElementsTuple> matchParcialSubjectObject(Integer predicate);

    public ArrayList<TwoElementsTuple> matchParcialSubjectPredicate(Integer object);

    public ArrayList<ThreeElementsTuple> matchTotal();
}
