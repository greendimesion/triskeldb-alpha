package triskeldb.storage;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import triskeldb.queries.query.ThreeElementsTuple;
import triskeldb.queries.query.TwoElementsTuple;
import triskeldb.facts.Fact;
import triskeldb.storage.factstorage.Cube;

public class FactStorage implements StorageManager, MatchingManager {

    Storage storage;

    public FactStorage() {
        try {
            this.storage = new Cube(50, 50, 50);
        } catch (Exception exception) {
            Logger logger = Logger.getLogger("CubeException");
            logger.log(Level.SEVERE, " " + exception.getMessage());
            Runtime.getRuntime().exit(0);
        }
    }

    @Override
    public Storage getStorageState() {
        return storage;
    }

    @Override
    public void setStorageState(Storage newStorage) {
        storage = newStorage;
    }

    @Override
    public void add(Fact fact) {
        try {
            storage.add(fact);
        } catch (Exception exception) {
            Logger logger = Logger.getLogger("CubeException");
            logger.log(Level.SEVERE, " " + exception.getMessage());
            Runtime.getRuntime().exit(0);
        }
    }

    @Override
    public void extract(Fact fact) {
        try {
            storage.extract(fact);
        } catch (Exception exception) {
            Logger logger = Logger.getLogger("CubeException");
            logger.log(Level.SEVERE, " " + exception.getMessage());
            Runtime.getRuntime().exit(0);
        }
    }

    @Override
    public boolean matchPattern(Integer subject, Integer predicate, Integer object) {
        return storage.matchPattern(subject, predicate, object);
    }

    @Override
    public ArrayList<Integer> matchParcialSubject(Integer predicate, Integer object) {
        return storage.matchParcialSubject(predicate, object);
    }

    @Override
    public ArrayList<Integer> matchParcialPredicate(Integer subject, Integer object) {
        return storage.matchParcialPredicate(subject, object);
    }

    @Override
    public ArrayList<Integer> matchParcialObject(Integer subject, Integer predicate) {
        return storage.matchParcialObject(subject, predicate);
    }

    @Override
    public ArrayList<TwoElementsTuple> matchParcialPredicateObject(Integer subject) {
        return storage.matchParcialPredicateObject(subject);
    }

    @Override
    public ArrayList<TwoElementsTuple> matchParcialSubjectObject(Integer predicate) {
        return storage.matchParcialSubjectObject(predicate);
    }

    @Override
    public ArrayList<TwoElementsTuple> matchParcialSubjectPredicate(Integer object) {
        return storage.matchParcialSubjectPredicate(object);
    }

    @Override
    public ArrayList<ThreeElementsTuple> matchTotal() {
        return storage.matchTotal();
    }

    @Override
    public ArrayList<Integer> matchParcialSubject(Integer predicate, ArrayList<Integer> object) {
        ArrayList<Integer> validSubjects = new ArrayList<>();
        for (int objectValue : object) {
            validSubjects.addAll(this.matchParcialSubject(predicate, objectValue));
        }
        return validSubjects;
    }
}
